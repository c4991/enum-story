package com.epam.enumstoryjava;
public enum AnimalE {
    CAT("клещи","кот"),DOG("сломана лапка", "собака"),HAMSTER("депрессия", "хомячок");
    private String disease;
    private String translation;
    AnimalE(String disease, String translation){
        this.disease = disease;
        this.translation = translation;
    }

    public String getDisease(){
        return disease;
    }

    public String getTranslation() {
        return translation;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }
}




