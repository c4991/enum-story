package com.epam.enumstoryjava;
public class Clinic {
    public static void main(String[] args) {
        Enum[] animals = AnimalE.values();
        AnimalE animal;
        String illness;
        String curable;
        for (int i=0; i < animals.length; i++){
            animal = (AnimalE) animals[i];
            illness = animal.getDisease();
            switch (illness){
                case "клещи":
                case "сломана лапка":
                    curable = "можем вылечить";
                    break;
                default:
                    curable = "не можем вылечить";
            }
            int number_in_queue = animal.ordinal()+1;
            System.out.println(animal.getTranslation() + " - " + curable + "; номер в очереди - " + number_in_queue);
        }
    }
}



